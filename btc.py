import chart_studio.plotly as py
import plotly.graph_objs as go
from plotly.tools import FigureFactory as FF

import numpy as np
import pandas as pd
import scipy
import requests
from binance.client import Client

api_key = ''
api_secret = ''

client = Client(api_key, api_secret)

# URL = "https://min-api.cryptocompare.com/data/histohour?fsym=BTC&tsym=USD&limit=400&api_key=79457ba8436236642a33d299329e7b97b2ad2a62e2efe7e8e2f5a40b60ad852c"
# r = requests.get(url = URL)
# data = r.json()
# price = data['Data'][0]['close']

klines = client.get_historical_klines("ETHBTC", Client.KLINE_INTERVAL_30MINUTE, "1 Dec, 2017", "1 Jan, 2018")
print (klines)


baseAmount = 1000
x = []
plots = []
amountOfBTC = []


def doTransaction(transactionTriggerPercent,amountYouEarned,lastCurrencyPrice, baseAmountAfterPriceChange, earnedAfterTimePeriod, baseAmount):
    global data
    global amountOfBTC

    for i in range(len(data['Data'])):
        price = data['Data'][i]['close']
        expectedGreaterPrice = baseAmount * (1 + transactionTriggerPercent)
        expectedLowerPrice = baseAmount * (1 - transactionTriggerPercent)
        if baseAmountAfterPriceChange >= expectedGreaterPrice:
            # print "aaa"
            baseAmountAfterPriceChange = baseAmountAfterPriceChange * (price/lastCurrencyPrice)
            amountYouEarned += (baseAmountAfterPriceChange - baseAmount)
            earnedAfterTimePeriod.append(amountYouEarned)
            lastCurrencyPrice = price
            baseAmountAfterPriceChange = baseAmount
        elif baseAmountAfterPriceChange <= expectedLowerPrice:
            # print "bbb"
            baseAmountAfterPriceChange = baseAmountAfterPriceChange * (price/lastCurrencyPrice)
            if baseAmount - baseAmountAfterPriceChange <= amountYouEarned:
                amountYouEarned -= (baseAmount - baseAmountAfterPriceChange)
                baseAmountAfterPriceChange = baseAmount
            else:
                baseAmountAfterPriceChange = baseAmountAfterPriceChange + amountYouEarned
                amountYouEarned = 0
            earnedAfterTimePeriod.append(amountYouEarned)
            lastCurrencyPrice = price
        else:
            earnedAfterTimePeriod.append(amountYouEarned)
            baseAmountAfterPriceChange = baseAmountAfterPriceChange * (price/lastCurrencyPrice)
            lastCurrencyPrice = price

        amountOfBTC.append(baseAmountAfterPriceChange / price)

    return earnedAfterTimePeriod




for i in range(len(data['Data'])):
    x.append(i)   

a = range(10,15,5)
for z in a:
    number = float(z)
    transactionTriggerPercent = number/1000
    amountYouEarned = 0
    lastCurrencyPrice = price
    baseAmountAfterPriceChange = baseAmount
    earnedAfterTimePeriod = []
      
    value = doTransaction(transactionTriggerPercent,amountYouEarned,lastCurrencyPrice, baseAmountAfterPriceChange, earnedAfterTimePeriod, baseAmount)

    trace = go.Scatter(
        x=x,
        y=value,
        mode='lines',
        name=z,
        marker=dict(
            size=5
        )
    )
    plots.append(trace)


trace2 = go.Scatter(
        x=x,
        y=amountOfBTC,
        mode='markers',
        name=z,
        marker=dict(
            size=5
        )
    )


layout = go.Layout(
    title='zyski w zaleznosci od procentarzu',
)


fig = go.Figure(data=trace2, layout=layout)
fig2 = go.Figure(data=plots, layout=layout)

fig.show()
fig2.show() 